
package org.cdi.advocacy;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class AtmMain {
        public static void main (String[] args) {
        	Date dt= Date.valueOf(LocalDate.now());
        	System.out.println("======>>>>>>"+dt.toGMTString());
        	//System.out.println("====>>>"+(LocalDateTime.now()).format(DateTimeFormatter.RFC_1123_DATE_TIME));
        	
        	System.out.println(DateTimeFormatter.RFC_1123_DATE_TIME.format(LocalDate.of(2018, 3, 9).atStartOfDay(ZoneId.of("UTC-3"))));
        }
}