package org.cdi.advocacy;

import java.math.BigDecimal;

import javax.inject.Inject;
import javax.inject.Named;

@Named("atm")
public class AutomatedTellerMachineImpl implements AutomatedTellerMachine {
    private ATMTransport transport;
    public void deposit(BigDecimal bd) {
        System.out.println("deposit called");
        transport.communicateWithBank(new byte[] {});
    }
    public void withdraw(BigDecimal bd) {
        System.out.println("withdraw called");
        transport.communicateWithBank(new byte[] {});
    }
    @Inject
	public void setTransport(ATMTransport transport2) {
		this.transport = transport2;
	}
}